import 'package:arkadia/Utils/Connection.dart';
import 'package:arkadia/Ventanas/ListaUsers.dart';
import 'package:arkadia/Ventanas/Login.dart';
import 'package:arkadia/Ventanas/Tablon.dart';
import 'package:flutter/material.dart';

void main() {
  Connection.getInstance().comprobarConexion();
  runApp(new MaterialApp(home: Login()));
}