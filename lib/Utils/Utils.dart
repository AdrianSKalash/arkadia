import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {

  static void openWA(String tlf, GlobalKey<ScaffoldState> key) async{
    var whatsappUrl ="whatsapp://send?phone=+34$tlf";
    await canLaunch(whatsappUrl)? launch(whatsappUrl):key.currentState.showSnackBar(
      SnackBar(content: Text("No tienes WhatsApp :("))
    );
  }

  static void callPhone(String tlf){
    launch("tel:$tlf");
  }


}