import 'dart:convert';

import 'package:arkadia/Utils/Connection.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:shared_preferences/shared_preferences.dart';




import '../Model/User.dart';

class Persistencia {

  static Future<bool> checkSaveUser()async{
    await Connection.getInstance().comprobarConexion();
    if(Connection.getInstance().conexion){
      String id = await Firebase.autoLogin();
      if(id != null){
        User actually = await Firebase.getUser(id);
        LocalUser.localUser = actually;
        await saveUser();
      }else{
        return false;
      }
    }else{
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userJSON = sp.get("LocalUser");
      if(userJSON != null){
        Map serializeUser = json.decode(userJSON);
        User old =User.fromJSON(serializeUser);
        LocalUser.localUser = old;
      }else{
        return false;
      }
    }
    return true;
  }

  static Future<void> saveUser()async{
    SharedPreferences sp = await SharedPreferences.getInstance();
    String jsonCode = json.encode(LocalUser.localUser.toJson());
    sp.setString("LocalUser", jsonCode);
  }
}