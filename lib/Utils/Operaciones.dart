import 'package:arkadia/Model/ObjectFather.dart';
import 'package:arkadia/Model/Tarea.dart';
import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Connection.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Ventanas/MensajeEdit.dart';
import 'package:arkadia/Ventanas/TareaEdit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

/// Clase que contiene las operaciones que pueden tener los
/// objetos.
class Operaciones {

  /// Obtiene las acciones principales de una tarea
  /// 
  /// Devuelve un [List] con las acciones principales de una tarea, 
  /// a partir de [item] dado.
  /// [isGeneral] sera [true] si estas en un tablon general o [false]
  /// si estas en el tablon personal 
  static List<Widget> getActions(Tareas item, BuildContext context,
      bool isGeneral) {

    /// Accion para entrar en una tarea
    IconSlideAction _entrar = IconSlideAction(
      icon: Icons.input,
      color: Colors.greenAccent,
      caption: "Entrar",
      onTap: () {
        if (Connection.getInstance().conexion) {
          Firebase.putIn(item);
        } else {
          Scaffold.of(context)
              .showSnackBar(Connection.getInstance().getSnackLostConnection());
        }
      },
    );

    // Accion para salir de una tarea
    IconSlideAction _salir = IconSlideAction(
      icon: Icons.call_missed_outgoing,
      color: Colors.purpleAccent,
      caption: "Salir",
      onTap: () {
        if (Connection.getInstance().conexion) {
          Firebase.putOut(item);
        } else {
          Scaffold.of(context)
              .showSnackBar(Connection.getInstance().getSnackLostConnection());
        }
      },
    );

    // Accion para completar una tarea
    IconSlideAction _completar = IconSlideAction(
      icon: Icons.check_circle,
      color: Colors.limeAccent,
      caption: "Completar",
      onTap: () {
        if (Connection.getInstance().conexion) {
          Firebase.complete(item);
        } else {
          Scaffold.of(context)
              .showSnackBar(Connection.getInstance().getSnackLostConnection());
        }
      },
    );
    
    if (isGeneral) {
      // Acciones para un tablon general
      if(item.prioridad != Prioridad.FINALIZADO && !item.responsables.
      containsKey(LocalUser.localUser.idUser)){
        // Solo podra entrar si no esta completada o no esta ya metido.
        return [_entrar];
      }else{
        return [];
      }
    }else{
      //Acciones para el tablon personal
      if(item.prioridad != Prioridad.FINALIZADO){
        // Podra salir o completar la tarea si no esta finalizada
        return [_salir,_completar];
      }else{
        return [];
      }
    }
  }

  /// Obtiene las acciones especiales de una tarea o mensaje
  /// 
  /// Estas acciones hacen falta permisos de admin, ser propietarios o que
  /// sean del kraalete
  static List<Widget> getSubActions(ObjectFather item, BuildContext context) {

    /// Accion de modificar un objeto
    IconSlideAction _modificar = IconSlideAction(
      icon: Icons.mode_edit,
      color: Colors.blue,
      caption: "Modificar",
      onTap: () {
        if (Connection.getInstance().conexion) {
          Widget wid;
          if (item is Tareas) {
            wid = (TareaEdit(item,item.isKraal));
          }else{
            wid = (MensajeEdit(item, item.isKraal));
          }
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => wid));
        } else {
          Scaffold.of(context)
              .showSnackBar(Connection.getInstance().getSnackLostConnection());
        }
      },
    );

    /// Accion para eliminar un objeto
    IconSlideAction _eliminar = IconSlideAction(
      icon: Icons.delete,
      color: Colors.red,
      caption: "Eliminar",
      onTap: () {
        if (Connection.getInstance().conexion) {
          Firebase.delete(item);
        } else {
          Scaffold.of(context)
              .showSnackBar(Connection.getInstance().getSnackLostConnection());
        }
      },
    );

    if (LocalUser.localUser.admin || LocalUser.localUser.idUser == item.propietario.keys.first
    || !item.isKraal) {
      return [_modificar,_eliminar];
    }else{
      return [];
    }

  }
}


