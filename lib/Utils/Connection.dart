import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';

/// Permite comprobar la conexion a la red
class Connection {
  static Connection _instance;
  /// Estado de la conexion.
  /// 
  /// Devuelve [true] si tiene conexion, o
  /// [false] si esta sin conexion.
  bool _conexion = false;

  Connection._private() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      print("Cambio de conexion a "+result.toString());
      _conexion = result != ConnectivityResult.none;
    });
  }


  bool get conexion => _conexion;

  /// Obtiene la instancia de la clase
  static Connection getInstance() {
    if (_instance == null) _instance = new Connection._private();
    return _instance;
  }

  /// Fuerza una comprobacion de la conexion
  Future<void> comprobarConexion() async {
    var connection = await (Connectivity().checkConnectivity());

    _conexion = connection != ConnectivityResult.none;
  }

  /// Muestra un mensaje de no tener conexion
  /// 
  /// Devuelve un [SnackBar] con un mensaje de 
  /// no tener conexion.
  SnackBar getSnackLostConnection(){
    return SnackBar(
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
        Icon(Icons.signal_wifi_off),
        Text("No hay conexion con la base de datos.")
      ],),
    );
  }
}
