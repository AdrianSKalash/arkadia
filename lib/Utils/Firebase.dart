import 'dart:typed_data';

import 'package:arkadia/Model/ObjectFather.dart';
import 'package:arkadia/Model/Tarea.dart';
import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:toast/toast.dart';


/// Clase que permite el manejo de Firebase
class Firebase{

  /// Referencia a la base de datos de FireStore
  static Firestore refDB = Firestore.instance;
  /// Referencia a la coleccion que contiene los usuarios
  static CollectionReference dbUser = refDB.collection("Usuarios");
  /// Referencia a la coleccion que contiene las tareas de kraal
  static CollectionReference dbTareas = refDB.collection("Tareas");
  /// Referencia a la coleccion que contiene los mensajes del kraal
  static CollectionReference dbMensajes = refDB.collection("Mensajes");
  /// Referencia a la coleccion que contiene los mensajes del kraalete del [LocalUser]
  static CollectionReference dbTareasKraalete = refDB.collection("Tareas"+Enums.getSeccion(LocalUser.localUser.seccion));
  /// referencia a la coleccion que contiene las tareas del kraalete del [LocalUser]
  static CollectionReference dbMensajesKraalete = refDB.collection("Mensajes"+Enums.getSeccion(LocalUser.localUser.seccion));
  static StorageReference dbStorage = FirebaseStorage.instance.ref();

  static final FirebaseAuth _auth = FirebaseAuth.instance;

  /// Añade un objeto a la base de datos.
  /// 
  /// Añade [objeto] a su respectiva coleccion de FireStore
  static void add(ObjectFather objeto){
    CollectionReference ref;
    if(objeto.isKraal){
      ref = objeto is Tareas?dbTareas:dbMensajes;
    }else{
      ref = objeto is Tareas?dbTareasKraalete:dbMensajesKraalete;
    }
    ref.document(objeto.id).setData(objeto.toJson()).catchError((e){print(e);});
  }

  /// Elimina un objeto de la base de datos
  /// 
  /// Elimina [objeto] de su respectiva coleccion de FireStore
  static void delete(ObjectFather objeto){
    CollectionReference ref;
    if(objeto.isKraal){
      ref = objeto is Tareas?dbTareas:dbMensajes;
    }else{
      ref = objeto is Tareas?dbTareasKraalete:dbMensajesKraalete;
    }
    ref.document(objeto.id).delete();
  }

  /// Obtiene una lista de Usuarios
  /// 
  /// Obtiene de FireStore una lista de [Users],
  /// a partir de una lista de [ids]
  static Future<List<User>> getUsers()async{
    List<User> lista = await dbUser.getDocuments().then((snapshot)async{
      return snapshot.documents.map((document) {
        User u =User.fromJSON(document.data);
        return u;
      }).toList();
    });

    for(User u in lista){
      var url = await Firebase.dbStorage.child(u.idUser+".jpg").getDownloadURL();
      u.foto = url.toString();
    }
    return lista;
  }

  /// Obtiene el usuario
  /// 
  /// Devuelve el [User] correspondiente a 
  /// la [id]
  static Future<User> getUser(String id)async{
    User u;
    await dbUser.document(id).get().then((datasnapshot)async{
      if(datasnapshot.exists){
        u = User.fromJSON(datasnapshot.data);
        var url = await Firebase.dbStorage.child(u.idUser+".jpg").getDownloadURL();
        u.foto = url.toString();
      }
    });
    return u;
  }

  /// Actualiza un objeto de la base de datos
  /// 
  /// Actualiza el [objeto] en su correspondiente coleccion 
  /// de FireStore
  static void update(ObjectFather objeto){
    CollectionReference ref;
    if(objeto.isKraal){
      ref = objeto is Tareas?dbTareas:dbMensajes;
    }else{
      ref = objeto is Tareas?dbTareasKraalete:dbMensajesKraalete;
    }
    ref.document(objeto.id).updateData(objeto.toJson()).catchError((e){print(e);});
  }

  /// Entra dentro de una tarea
  /// 
  /// Inserta al [LocalUser] en los responsables de la
  /// tarea [t] dentro de FireStore
  /// Si no habia nadie metido en la tarea, cambiara su
  /// prioridad a [EN_PROCESO]
  static void putIn(Tareas t){
    CollectionReference ref = t.isKraal?dbTareas:dbTareasKraalete;
    User user = LocalUser.localUser;
    ref.document(t.id).updateData({"responsables."+user.idUser : user.nombre}).then((voi){
      if(t.prioridad != Prioridad.EN_PROCESO){
        ref.document(t.id).updateData({"prioridad" : Prioridad.EN_PROCESO.toString()});
      }
    });
  }

  /// Sale de una tarea
  /// 
  /// Elimina al [LocalUser] de los responsables de la
  /// tarea [t] dentro de FireStore.
  /// Si no queda nadie mas dentro de la tarea, cambiara 
  /// su prioridad a [PENDIENTE]
  static void putOut(Tareas t){
    CollectionReference ref = t.isKraal?dbTareas:dbTareasKraalete;
    String user = LocalUser.localUser.idUser;
    ref.document(t.id).updateData({"responsables."+user : FieldValue.delete()}).then((voi){
      if((t.responsables.length - 1) == 0){
        ref.document(t.id).updateData({"prioridad" : Prioridad.PENDIENTE.toString()});
      }
    });
  }

  /// Completa una tarea
  /// 
  /// Marca la tarea [t] como completada, cambiandole su
  /// prioridad a [FINALIZADO].
  /// Tambien le añade la fecha de finalizacion.
  static void complete(Tareas t){
    CollectionReference ref = t.isKraal?dbTareas:dbTareasKraalete;
    DateTime fin = DateTime.now();
    ref.document(t.id).updateData({
      "prioridad" : Prioridad.FINALIZADO.toString(),
      "fechaFinalizacion" : fin
    });
  }

  static Future<String> signIn(String email, String pass)async{
    FirebaseUser user;
    String s;
    try {
      user = await _auth.signInWithEmailAndPassword(email: email, password: pass);
    } catch (e) {
      print(e.toString());
    }finally{
      if(user != null){
        return user.uid;
      }else{
        return null;
      }
    }
  }

  static Future<String> autoLogin()async{
    FirebaseUser user = await _auth.currentUser();
    if(user != null){
      return user.uid;
    }else{
      return null;
    }
  }

  static Future<void> signOff()async{
    try{
      await _auth.signOut();
    }catch(e){
      print(e.toString());
    }
  }

  static Future<void> resetPassword(context)async{
    try{
      FirebaseUser user = await _auth.currentUser();
      await _auth.sendPasswordResetEmail(email: user.email);
      print("change");
      Toast.show("Se ha enviado un email", context);
    }catch(e){
      print(e.toString());
    }
  }

}