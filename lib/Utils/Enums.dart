
import 'package:flutter/material.dart';

/// Indica la Seccion
/// 
/// Puede valer [MANADA], [TROPA], [ESCULTAS] O [CLAN]
enum Seccion { MANADA, TROPA, ESCULTAS, CLAN }

/// Indica el cargo
/// 
/// Puede valer [COR] para Coordinador, [TES] para Tesorero,
/// [BOD] para Bodeguero, [NEX] para Nexo, [COM] para Comunicaciones,
///  [SEC] para Secretario, [SAN] para Sanitario O [NULL] para 
/// indicar que no pertenece a ningun cargo
enum Cargo { COR, TES, BOD, NEX, COM, SEC, SAN, NULL }

/// Indica la Prioridad
/// 
/// Puede valer [EN_PROCESO], [PENDIENTE], [URGENTE] O [FINALIZADO]
enum Prioridad { EN_PROCESO, PENDIENTE, URGENTE, FINALIZADO }

/// Clase para trabajar con los enums
class  Enums{

    /// Obtiene el cargo
    /// 
    /// Devuelve la definicion del Cargo [c] 
    static String getCargo(Cargo c){
      String s;
      switch(c){
        case Cargo.BOD:
          s = "Bodegueria";
          break;
        case Cargo.COM:
          s = "Comunicacion";
          break;
        case Cargo.COR:
          s = "Coordinacion";
          break;
        case Cargo.NEX:
          s = "Nexo";
          break;
        case Cargo.SEC:
          s = "Secretaria";
          break;
        case Cargo.TES:
          s = "Tesoreria";
          break;
        case Cargo.SAN:
          s = "Sanitario";
          break;
        case Cargo.NULL:
          s = "Sin Cargo";
          break;
      }
      return s;
    }

    /// Obtiene el nivel de la prioridad
    /// 
    /// Devuelve el nivel de importancia de la prioridad [p] 
    static int getPrioridadValue(Prioridad p){
      int value;
      switch(p){
        case Prioridad.EN_PROCESO:
          value = 2;
          break;
        case Prioridad.PENDIENTE:
          value = 1;
          break;
        case Prioridad.URGENTE:
          value = 0;
          break;
        case Prioridad.FINALIZADO:
          value = 3;
          break;
      }
      return value;
    }

    /// Obtiene la prioridad
    /// 
    /// Devuelve el color correspondiente de la prioridad [p] 
    static Color getPrioridad(Prioridad p){
      Color c;
      switch(p){
        case Prioridad.EN_PROCESO:
          c = Colors.greenAccent;
          break;
        case Prioridad.PENDIENTE:
          c = Colors.yellowAccent;
          break;
        case Prioridad.URGENTE:
          c = Colors.redAccent;
          break;
        case Prioridad.FINALIZADO:
          c = Colors.grey;
          break;
      }
      return c;

    }

    /// Obtiene la seccion
    /// 
    /// Devuelve la definicion de la seccion [p] 
    static String getSeccion(Seccion s){
      String st;
      switch(s){
        case Seccion.MANADA:
          st = "Manada";
          break;
        case Seccion.TROPA:
          st = "Tropa";
          break;
        case Seccion.ESCULTAS:
          st = "Escultas";
          break;
        case Seccion.CLAN:
          st = "Clan";
          break;
      }
      return st;
    }

    /// Obtiene el enum correspondiente a un Sting
    /// 
    /// Obtiene el enum correspondiente al [name], pasandole
    /// los valores del enum en la [list]
    static  T getEnum<T>(List<T> list, String name){
        for(T elem in list){
          if(elem.toString() == name){
            return elem;
          }
        }
    }
  }
