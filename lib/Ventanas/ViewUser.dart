import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Ventanas/Login.dart';
import 'package:arkadia/Ventanas/Tutorial.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';

class ViewUser extends StatefulWidget {
  @override
  _ViewUserState createState() => _ViewUserState();
}

class _ViewUserState extends State<ViewUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(LocalUser.localUser.nombre),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.help_outline),
              onPressed: () =>
                  Tutorial.getTutorial(Tutorial.tutorialEdicion, context),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20, left: 50, right: 50),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  CircleAvatar(
                    radius: 60,
                    backgroundImage: AdvancedNetworkImage(
                      LocalUser.localUser.foto,
                      useDiskCache: true,
                      cacheRule: CacheRule(maxAge: Duration(days: 7)),
                    ),
                  ),
                  Divider(
                    height: 50,
                    color: Colors.transparent,
                  ),
                  SizedBox(
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Table(
                          defaultColumnWidth: FixedColumnWidth(100),
                          children: [
                            TableRow(children: [
                              TableCell(child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: Text("Nombre: "),
                              )),
                              TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 10),
                                    child: Text(LocalUser.localUser.nombre),
                                  )),
                            ]),
                            TableRow(children: [
                              TableCell(child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: Text("Seccion: "),
                              )),
                              TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 10),
                                    child: Text(Enums.getSeccion(
                                        LocalUser.localUser.seccion)),
                                  )),
                            ]),
                            TableRow(children: [
                              TableCell(child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: Text("Admin: "),
                              )),
                              TableCell(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 10),
                                    child: Text(
                                        LocalUser.localUser.admin ? "Si" : "No"),
                                  )),
                            ]),
                            TableRow(children: [
                              TableCell(child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: Text("Telefono: "),
                              )),
                              TableCell(child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: Text(LocalUser.localUser.tlf),
                              )),
                            ]),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Divider(
                    height: 50,
                    color: Colors.transparent,
                  ),
                  RaisedButton(
                    child: Text("Desconectar"),
                    onPressed: _signOff,
                  ),
                  Divider(
                    height: 20,
                    color: Colors.transparent,
                  ),
                  GestureDetector(
                    onTap: () => Firebase.resetPassword(context),
                    child: Text("Cambiar de contraseña",
                    style: TextStyle(
                      color: Colors.blueAccent,
                      decoration: TextDecoration.underline,
                    ),),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  void _signOff()async{
    await Firebase.signOff();
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => Login()), ModalRoute.withName('/'),);
  }
}
