import 'package:arkadia/Model/Mensaje.dart';
import 'package:arkadia/Model/ObjectFather.dart';
import 'package:arkadia/Model/Tarea.dart';
import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Connection.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Ventanas/MensajeEdit.dart';
import 'package:arkadia/Ventanas/TareaEdit.dart';
import 'package:arkadia/Ventanas/Tutorial.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

/// Tablon general
/// 
/// Dependiendo de [isKraal] puede ser de
/// Kraal o del Kraalete
class General extends StatefulWidget {
  bool _isKraal;

  General(this._isKraal);

  @override
  _GeneralState createState() => _GeneralState(_isKraal);
}

class _GeneralState extends State<General>
    with SingleTickerProviderStateMixin {

  bool _isKraal;

  _GeneralState(this._isKraal);

  TabController _tab;

  Stream<QuerySnapshot> tareas;
  Stream<QuerySnapshot> mensajes;
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    super.initState();
    // Inicia el Tab, obtiene el snapshot de la referencia, dependiendo
    // de si es el tablon de kraal o de kraalete
    _tab = TabController(length: 2, vsync: this);
    if(_isKraal){
      tareas = Firebase.dbTareas.snapshots();
      mensajes = Firebase.dbMensajes.snapshots();
    }else{
      tareas = Firebase.dbTareasKraalete.snapshots();
      mensajes = Firebase.dbMensajesKraalete.snapshots();
    }

    // Crea unos listen para forzar la recarga de cambios cuando
    // FireStore detecte un cambio
    tareas.listen((query) => reloadTareas());
    mensajes.listen((query) => reloadMensajes());
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: FloatingActionButton(     //Boton de añadir objeto
          shape: StadiumBorder(),
          backgroundColor: Colors.blueAccent,
          child: Icon(Icons.add),
          onPressed: _floatingButtomAction),
      appBar: AppBar(
        title: Text(_isKraal?"Kraal":"Kraalete de "+Enums.getSeccion(LocalUser.localUser.seccion)),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.help_outline),
            onPressed: () => Tutorial.getTutorial(Tutorial.tutorialTareas, context),
          ),
        ],
        bottom: TabBar(
            controller: _tab,
            tabs: [
              Tab(
                text: "Tareas",
                icon: Icon(Icons.check_box),
              ),
              Tab(
                text: "Mensajes",
                icon: Icon(Icons.mail),
              )
            ]),
      ),
      body: TabBarView(
        physics: NeverScrollableScrollPhysics(),
          controller: _tab,
          children: [
            RefreshIndicator(                                     //Tablon de tareas
              onRefresh: reloadTareas,
              child: StreamBuilder<QuerySnapshot>(              
                stream: tareas,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) return new Text('${snapshot.error}');
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return new Center(child: CircularProgressIndicator());
                    default:
                      List<Tareas> datos = snapshot.data.documents.map(
                              (snapshot)=>Tareas.fromJSON(snapshot.data)).toList();
                      datos.sort((a,b) => Enums.getPrioridadValue(a.prioridad).
                      compareTo(Enums.getPrioridadValue(b.prioridad)));
                      return ListView(
                        padding: EdgeInsets.all(20.0),
                        children: datos.map((
                            Tareas document) {
                          return TareaCard(document,true);
                        }).toList(),
                      );
                  }
                },
              ),
            ),
            RefreshIndicator(                                     // Tablon de mensajes
              onRefresh: reloadMensajes,
              child: StreamBuilder<QuerySnapshot>(
                stream: mensajes,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) return new Text('${snapshot.error}');
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return new Center(child: CircularProgressIndicator());
                    default:
                      return ListView(
                        padding: EdgeInsets.all(20.0),
                        children: snapshot.data.documents.map((
                            DocumentSnapshot document) {
                          return MensajeCard(Mensaje.fromJSON(document.data));
                        }).toList(),
                      );
                  }
                },
              ),
            ),
          ]),

    );
  }

  /// Fuerza la recarga del tablon de Tareas
  Future<Null> reloadTareas() async{
    if(mounted){
      return await setState(() {
       tareas = _isKraal?Firebase.dbTareas.snapshots():Firebase.dbTareasKraalete.snapshots();
      });
    }
  }
  /// Fuerza la recarga del tablon de Mensajes
  Future<Null> reloadMensajes() async{
    if(mounted) {
      return await setState(() {
        mensajes = _isKraal?Firebase.dbMensajes.snapshots():Firebase.dbMensajesKraalete.snapshots();
      });
    }
  }

  /// Permite ir al editor
  /// 
  /// Si hay conexion envia al editor correspondiente,
  /// si no muestra un mensaje denegando la accion
  void _floatingButtomAction() {
    Widget page = null;
    if (_tab.index == 0) {
      page = TareaEdit.empty(_isKraal);
    } else {
      page = MensajeEdit.empty(_isKraal);
    }

    if (Connection.getInstance().conexion) {
      Navigator.of(context).push(
          MaterialPageRoute(builder: (BuildContext context) => page));
    } else {
      _scaffoldKey.currentState
          .showSnackBar(Connection.getInstance().getSnackLostConnection());
    }


  }
}
