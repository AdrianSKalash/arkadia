import 'package:arkadia/Model/Mensaje.dart';
import 'package:arkadia/Model/Tarea.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Ventanas/General.dart';
import 'package:arkadia/Ventanas/Tutorial.dart';
import 'package:flutter/material.dart';

/// Pagina para crear/editar un mensaje
/// 
/// Se crea un mensaje o se edita el mensaje
/// [mensaje]
/// [isKraal] define si es mensaje de Kraal o
///  de Kraalete
class MensajeEdit extends StatefulWidget {
  Mensaje _mensaje;
  _MensajeEditState _state;
  bool _isKraal;

  /// Cosntructor para editar un mensaje
  MensajeEdit(this._mensaje,this._isKraal) {
    _state = _MensajeEditState(_mensaje,_isKraal);
  }

  /// Constructor para crear un mensaje
  MensajeEdit.empty(this._isKraal) {
    _state = _MensajeEditState.empty(_isKraal);
  }

  @override
  _MensajeEditState createState() => _state;
}

class _MensajeEditState extends State<MensajeEdit> {
  bool _isKraal;
  _MensajeEditState(this._mensaje,this._isKraal);

  _MensajeEditState.empty(this._isKraal);

  Mensaje _mensaje;
  Cargo _cargo = Cargo.NULL;
  TextEditingController _nombre = TextEditingController();
  TextEditingController _descripcion = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    // Si estamos editando, introduce los datos del mensaje
    if (_mensaje != null) {
      _nombre.text = _mensaje.nombre;
      _cargo = _mensaje.cargo;
      _descripcion.text = _mensaje.mensaje;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       // resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text("Tarea"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.help_outline),
              onPressed: () => Tutorial.getTutorial(Tutorial.tutorialEdicion, context),
            ),
          ],
        ),
        body: Form(
          key: _formKey,
          child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("Nombre:"),                                      //Nombre //NotNULL
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.only(left: 20, top: 20),
                          child: TextFormField(
                            maxLines: 1,
                            decoration: InputDecoration(
                                hintText: "Nombre",
                                contentPadding: EdgeInsets.all(10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0))),
                            controller: _nombre,
                            maxLength: 50,
                            validator: (value) {
                              if (value.isEmpty) {
                                return "No puede estar el nombre vacio";
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    height: 50,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Cargo"),                                          //Cargo
                      DropdownButton<Cargo>(
                        value: _cargo,
                        items: Cargo.values
                            .map((Cargo cargo) => DropdownMenuItem<Cargo>(
                                value: cargo,
                                child: Text(Enums.getCargo(cargo))))
                            .toList(),
                        onChanged: (cargo) {
                          setState(() {
                            _cargo = cargo;
                          });
                        },
                      ),
                    ],
                  ),
                  Divider(
                    height: 50,
                  ),
                  Text("Mensaje"),                                            //Cuerpo del mensaje //NotNull
                  Container(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: 150.0,
                      ),
                      child: Scrollbar(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: TextFormField(
                              controller: _descripcion,
                              maxLines: null,
                              maxLength: 200,
                              decoration: InputDecoration(
                                  hintText: "Escribe la descripcion (Opcional)",
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(width: 10),
                                      gapPadding: 20)),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Tiene que haber un mensaje";
                                }
                              }),
                        ),
                      ),
                    ),
                  ),                                                            //Boton aceptar
                  Center(
                    child: RaisedButton(
                        child: Text("Aceptar"),
                        onPressed: _onPressButtonAceptar),
                  ),
                ],
              )),
        ));
  }

  /// Valida el formulario, y crea o actualiza el mensaje
  void _onPressButtonAceptar() {
    if (_formKey.currentState.validate()) {
      if (_mensaje != null) {
        _mensaje.nombre = _nombre.text;
        _mensaje.mensaje = _descripcion.text;
        _mensaje.cargo = _cargo;

        Firebase.update(_mensaje);
      } else {
        _mensaje =
            new Mensaje(_nombre.text, _descripcion.text, _cargo, _isKraal);
        Firebase.add(_mensaje);
      }

      Navigator.of(context).pop();
    }
  }
}
