import 'package:arkadia/Model/Tarea.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Ventanas/General.dart';
import 'package:arkadia/Ventanas/Tutorial.dart';
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';

/// Pagina para crear/editar una tarea
///
/// Se crea un tarea o se edita la tarea
/// [tarea]
/// [isKraal] define si es tarea de Kraal o
///  de Kraalete
class TareaEdit extends StatefulWidget {
  Tareas _tarea;
  _TareaEditState _state;
  bool _isKraal;

  TareaEdit(this._tarea, this._isKraal) {
    _state = _TareaEditState(_tarea, this._isKraal);
  }

  TareaEdit.empty(this._isKraal) {
    _state = _TareaEditState.empty(_isKraal);
  }

  @override
  _TareaEditState createState() => _state;
}

class _TareaEditState extends State<TareaEdit> {
  bool _isKraal;

  _TareaEditState(this._tarea, this._isKraal);

  _TareaEditState.empty(this._isKraal);

  Tareas _tarea;
  DateTime _fechaLimite;
  Cargo _cargo = Cargo.NULL;
  TextEditingController _nombre = TextEditingController();
  TextEditingController _descripcion = TextEditingController();
  Prioridad _prioridad = Prioridad.PENDIENTE;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    // Si estamos editando, insertamos los datos de la tarea
    if (_tarea != null) {
      _fechaLimite = _tarea.fechaLimite;
      _nombre.text = _tarea.nombre;
      _cargo = _tarea.cargo;
      _descripcion.text = _tarea.descripcion;
      _prioridad = _tarea.prioridad;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //resizeToAvoidBottomPadding: false,
        appBar: AppBar(title: Text("Tarea"),
            actions: <Widget>[
          IconButton(
            icon: Icon(Icons.help_outline),
            onPressed: () =>
                Tutorial.getTutorial(Tutorial.tutorialEdicion, context),
          ),
        ]),
        body: Form(
          key: _formKey,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(
                children: [
                  Row(
                    //Nombre //NotNULL
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("Nombre:"),
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.only(left: 20, top: 20),
                          child: TextFormField(
                            maxLines: 1,
                            decoration: InputDecoration(
                                hintText: "Nombre",
                                contentPadding: EdgeInsets.all(10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20.0))),
                            controller: _nombre,
                            maxLength: 50,
                            validator: (value) {
                              if (value.isEmpty) {
                                return "No puede estar el nombre vacio";
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    height: 10,
                  ),
                  Row(
                    //Cargo
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Cargo"),
                      DropdownButton<Cargo>(
                        value: _cargo,
                        items: Cargo.values
                            .map((Cargo cargo) => DropdownMenuItem<Cargo>(
                                value: cargo,
                                child: Text(Enums.getCargo(cargo))))
                            .toList(),
                        onChanged: (cargo) {
                          setState(() {
                            _cargo = cargo;
                          });
                        },
                      ),
                    ],
                  ),
                  Divider(
                    height: 50,
                  ),
                  Row(
                    //Prioridad
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Prioridad"),
                      Column(
                        children: <Widget>[
                          Text("Normal"),
                          Radio<Prioridad>(
                            value: Prioridad.PENDIENTE,
                            groupValue: _prioridad,
                            onChanged: _onChangeRadioButton,
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text("Urgente"),
                          Radio<Prioridad>(
                            value: Prioridad.URGENTE,
                            groupValue: _prioridad,
                            onChanged: _onChangeRadioButton,
                          ),
                        ],
                      )
                    ],
                  ),
                  Divider(
                    height: 50,
                  ),
                  Row(
                    //Fecha limite
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Fecha limite"),
                      InkWell(
                        child: Container(
                          height: 50,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          decoration: BoxDecoration(
                              color: Colors.lightBlueAccent,
                              border: Border.all(
                                  color: Colors.blueAccent, width: 2),
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Text(
                              _fechaLimite != null
                                  ? formatDate(
                                      _fechaLimite, [dd, '-', mm, '-', yy])
                                  : "Sin fecha",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        onTap: () => _selectDate(context),
                        onLongPress: () {
                          setState(() {
                            _fechaLimite = null;
                          });
                        },
                      ),
                    ],
                  ),
                  Divider(
                    height: 50,
                  ),
                  Text("Descripcion"),
                  Container(
                    //Descripcion
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: 150.0,
                      ),
                      child: Scrollbar(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: TextField(
                            controller: _descripcion,
                            maxLines: null,
                            maxLength: 200,
                            decoration: InputDecoration(
                                hintText: "Escribe la descripcion (Opcional)",
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(width: 10),
                                    gapPadding: 20)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    //Boton de aceptar
                    child: RaisedButton(
                        child: Text("Aceptar"),
                        onPressed: _onPressButtonAceptar),
                  ),
                ],
              )),
        ));
  }

  /// OnChange del radio button de prioridad
  void _onChangeRadioButton(Prioridad prioridad) {
    setState(() {
      _prioridad = prioridad;
    });
  }

  /// Muestra un DatePicker para la fecha de finalizacion
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime fecha = await showDatePicker(
        context: context,
        initialDate: DateTime.now().add(Duration(days: 1)),
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 360)));
    if (fecha != null) {
      setState(() {
        _fechaLimite = fecha;
      });
    }
  }

  /// Valida el formulario e inserta o actualiza la tarea
  void _onPressButtonAceptar() {
    if (_formKey.currentState.validate()) {
      if (_tarea != null) {
        _tarea.nombre = _nombre.text;
        _tarea.descripcion = _descripcion.text;
        _tarea.cargo = _cargo;
        _tarea.prioridad = _prioridad;
        _tarea.fechaLimite = _fechaLimite;
        Firebase.update(_tarea);
      } else {
        _tarea = new Tareas(_nombre.text, _descripcion.text, _cargo, _prioridad,
            _fechaLimite, _isKraal);
        Firebase.add(_tarea);
      }

      Navigator.of(context).pop();
    }
  }
}
