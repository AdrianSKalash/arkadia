import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Utils/Persistencia.dart';
import 'package:arkadia/Ventanas/Tablon.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _user = TextEditingController();
  TextEditingController _pass = TextEditingController();
  bool _isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isLoading = true;
    _checkLocalUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: ListTile(
          title: Text(
            "Registro",
            style: TextStyle(color: Colors.white),
          ),
          leading: Icon(
            Icons.person_pin,
            color: Colors.white,
          ),
        ),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        dismissible: false,
        progressIndicator: CircularProgressIndicator(),
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/Fondo.JPG"),
                  fit: BoxFit.fill,
                  repeat: ImageRepeat.repeatX)),
          child: Form(
            key: _formKey,
            child: Center(
              child: Card(
                elevation: 10,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25),
                    side: BorderSide(width: 3, color: Colors.blue)),
                margin: EdgeInsets.all(50),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child: TextFormField(
                        controller: _user,
                        decoration: InputDecoration(
                            labelText: "Usuario",
                            icon: Icon(Icons.person),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                        validator: (text) {
                          if (text.isEmpty) {
                            return "No puede estar vacio";
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child: TextFormField(
                        controller: _pass,
                        obscureText: true,
                        decoration: InputDecoration(
                            labelText: "Contraseña",
                            icon: Icon(Icons.enhanced_encryption),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20))),
                        validator: (text) {
                          if (text.isEmpty) {
                            return "No puede estar vacio";
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                            color: Colors.lightBlue,
                            child: Text("Aceptar"),
                            onPressed: _validarUser),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _checkLocalUser() async {
    if (await Persistencia.checkSaveUser()) {
      print("1");
      setState(() {
        setState(() => _isLoading = false);
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (BuildContext context) => Tablon()));
      });
    } else {
      print("2");
      setState(() => _isLoading = false);
    }
  }

  void _validarUser() async {
    setState(() => _isLoading = true);
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      String id = await Firebase.signIn(_user.text, _pass.text);
      if (id != null) {
        User user = await Firebase.getUser(id);
        LocalUser.localUser = user;
        await Persistencia.saveUser();
        setState(() => _isLoading = false);
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (BuildContext context) => Tablon()));
      } else {
        setState(() => _isLoading = false);
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text("Error de login")));
      }
    }
  }
}
