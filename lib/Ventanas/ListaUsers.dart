import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Utils/Utils.dart';
import 'package:arkadia/Ventanas/Tutorial.dart';
import 'package:flutter/material.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ListaUsers extends StatefulWidget {
  @override
  _ListaUsersState createState() => _ListaUsersState();
}

class _ListaUsersState extends State<ListaUsers> {

  List<User> _users = List();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Image _foto;

  @override
  void initState() {
    super.initState();
    _reload();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Miembros"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.help_outline),
              onPressed: () =>
                  Tutorial.getTutorial(Tutorial.tutorialListado, context),
            ),
          ]
      ),
      body: RefreshIndicator(
        onRefresh: _reload,
        child: ListView.builder(
          itemCount: _users.length,
          itemBuilder: (BuildContext context, int index){
            User u = _users[index];
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: GroovinExpansionTile(
                boxDecoration: BoxDecoration(
                    borderRadius:BorderRadius.circular(10),
                  border: Border.all(width: 1)
                ),
                trailing: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 4, color: Colors.blueAccent)
                  ),
                  child: Icon(Icons.keyboard_arrow_down,color: Colors.blueAccent,)),
                subtitle: Text(Enums.getSeccion(u.seccion)),
                title: Text(u.nombre),
                leading: Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AdvancedNetworkImage(
                          u.foto,
                        useDiskCache: true,
                        cacheRule: CacheRule(maxAge: Duration(days: 7))
                      ),
                    ),
                  ),
                ),
                children: <Widget>[
                  Divider(),
                  Text("Telefono: "+u.tlf),
                  Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(width: 5, color: Colors.blueAccent),
                            borderRadius: BorderRadius.circular(10)
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(Icons.call, color: Colors.blueAccent,),
                        ),
                        onTap: ()=>Utils.callPhone(u.tlf),
                      ),
                      InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(width: 5, color: Colors.blueAccent),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          padding: const EdgeInsets.all(10.0),
                          child: Icon(MdiIcons.whatsapp, color: Colors.blueAccent,),
                        ),
                        onTap: ()=>Utils.openWA(u.tlf,_scaffoldKey),
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> _reload() async {
    Firebase.getUsers().then((listaUsers){
      setState(() {
        _users = listaUsers;
      });
    });

  }

}
