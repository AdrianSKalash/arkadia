import 'package:arkadia/Model/Tarea.dart';
import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Ventanas/General.dart';
import 'package:arkadia/Ventanas/ListaUsers.dart';
import 'package:arkadia/Ventanas/Login.dart';
import 'package:arkadia/Ventanas/Tutorial.dart';
import 'package:arkadia/Ventanas/ViewUser.dart';
import 'package:async/async.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

/// Tablon personal
class Tablon extends StatefulWidget {
  @override
  _TablonState createState() => _TablonState();
}

class _TablonState extends State<Tablon> {
  Stream<List<QuerySnapshot>> tareas;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Stream<QuerySnapshot> stream1 = Firebase.dbTareas
        .where("responsables." + LocalUser.localUser.idUser, isGreaterThan: "")
        .snapshots();
    Stream<QuerySnapshot> stream2 = Firebase.dbTareasKraalete
        .where("responsables." + LocalUser.localUser.idUser, isGreaterThan: "")
        .snapshots();
    tareas = _getData();
    stream1.listen((query) {
      _reload();
    });
    stream2.listen((query) {
      _reload();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: Text("Tablon"),
          backgroundColor: Colors.blueAccent,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.help_outline),
              onPressed: () =>
                  Tutorial.getTutorial(Tutorial.tutorialTablon, context),
            ),
          ],
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: Container(
                  padding: EdgeInsets.only(top: 2,bottom: 2,right: 4,left: 4),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black,width: 2),
                      borderRadius: BorderRadius.circular(20),
                    color: Colors.white
                  ),
                  child: Text(
                      LocalUser.localUser.nombre,
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                ),
                accountEmail:
                    Container(
                        padding: EdgeInsets.only(top: 2,bottom: 2,right: 4,left: 4),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black,width: 2),
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white
                        ),
                        child: Text(
                            Enums.getSeccion(LocalUser.localUser.seccion),
                          style: TextStyle(color: Colors.blueAccent),
                        )
                    ),
                currentAccountPicture: CircleAvatar(
                  child: Container(
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                      border: Border.all(width: 5,color: Color.fromARGB(255, 15, 48, 172)),
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AdvancedNetworkImage(LocalUser.localUser.foto,
                            useDiskCache: true,
                            cacheRule: CacheRule(maxAge: Duration(days: 7))),
                      ),
                    ),
                  ),
                ),
                otherAccountsPictures: <Widget>[
                  CircleAvatar(
                    backgroundImage: AssetImage("images/LogoAmaranto.png"),
                  ),
                ],
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("images/Fondo.JPG"),
                    repeat: ImageRepeat.repeatX
                  )
                  //image:
                ),
              ),
              ListTile(
                title: Text("Kraal"),
                trailing: Icon(MdiIcons.accountGroup),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(_navigate(General(true)));
                },
              ),
              ListTile(
                title: Text("Kraalete"),
                trailing: Icon(MdiIcons.accountMultiple),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(_navigate(General(false)));
                },
              ),
              ListTile(
                title: Text("Miembros"),
                trailing: Icon(MdiIcons.formatListBulleted),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(_navigate(ListaUsers()));
                },
              ),
              Divider(),
              ListTile(
                title: Text("Usuario"),
                trailing: Icon(MdiIcons.account),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(_navigate(ViewUser()));
                },
              ),
              Divider(),
              ListTile(
                title: Text("Cancelar"),
                trailing: Icon(Icons.cancel),
                onTap: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
        body: StreamBuilder(
          stream: tareas,
          builder: (BuildContext context,
              AsyncSnapshot<List<QuerySnapshot>> snapshots) {
            if (snapshots.hasError) return new Text('${snapshots.error}');
            switch (snapshots.connectionState) {
              case ConnectionState.waiting:
                return new Center(child: CircularProgressIndicator());
              default:
                List<QuerySnapshot> querySnapshotData = snapshots.data.toList();
                List<DocumentSnapshot> lista = List<DocumentSnapshot>();
                lista.addAll(querySnapshotData[0].documents);
                lista.addAll(querySnapshotData[1].documents);
                return RefreshIndicator(
                  onRefresh: _reload,
                  child: ListView(
                    padding: EdgeInsets.all(20.0),
                    children: lista.map((DocumentSnapshot document) {
                      return TareaCard(Tareas.fromJSON(document.data), false);
                    }).toList(),
                  ),
                );
            }
          },
        ));
  }

  Future<Null> _reload() async {
    if (this.mounted) {
      return await setState(() {
        print("aaa");
        tareas = _getData();
      });
    }
  }

  MaterialPageRoute _navigate(Widget page) {
    return MaterialPageRoute(builder: (BuildContext context) => page);
  }

  Stream<List<QuerySnapshot>> _getData() {
    Stream<QuerySnapshot> stream1 = Firebase.dbTareas
        .where("responsables." + LocalUser.localUser.idUser, isGreaterThan: "")
        .snapshots();
    Stream<QuerySnapshot> stream2 = Firebase.dbTareasKraalete
        .where("responsables." + LocalUser.localUser.idUser, isGreaterThan: "")
        .snapshots();

    return StreamZip([stream1, stream2]);
  }
}
