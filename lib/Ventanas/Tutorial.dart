import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:introduction_screen/model/page_view_model.dart';

class Tutorial{

    static List<PageViewModel> tutorialEdicion = [
      PageViewModel("Intro", "Aprenderemos a usar el editor para crear nuevas tareas o editar las existentes", null),
      PageViewModel("Nombre", "¿En seruio hace falta que diga que en nombre se pone el nombre de la tarea? \nEso si, es obligatorio.", null),
      PageViewModel("Cargo", "Puedes elegir en que cargo esta asignada la tarea.\nO puedes no ponerle cargo, así, como si vivieras en la anarquia.", null),
      PageViewModel("Prioridad", "Elige que prioridad quieres que tenga la tarea, entre Normal o Urgente.\nNo te emociones mucho que solo le cambia el color, no va a venir un angel a completar la tarea ni nada parecido..", null),
      PageViewModel("Fecha limite", "Nada como poner una fecha limite para estresar al projimo.\nPulsa el boton para introducir fecha, o mantenlo puldado para quitar la fecha.", null),
      PageViewModel("Descripcion", "Puedes meter una descripcion para detallar la tarea. \nO no, que se jod** y tengan que adivinar lo que quieres que hagan.", null),
      PageViewModel("Aceptar", "Igual es necesario que explique el boton de Aceptar.\nSirve para servirte un cafe", null),
    ];



  static List<PageViewModel> tutorialTareas = [
      PageViewModel("Intro", "Aqui se ven todas las tareas ordenadas por prioridad, o los mensajes.", null),
      PageViewModel("Prioridades (Solo tareas)", "Rojo significa que es urgente.\nAmarillo tiene prioridad normal.\nVerde es que ya hay personas con ello.\nGris es que se ha finalizado", null),
      PageViewModel("Acciones principales (Solo tareas)", "Se ven si deslizas a la derecha.\nLa unica accion que puedes hacer aqui es meterte en la tare, a no ser que ya estes dentro o que se haya finalizado.", null),
      PageViewModel("Acciones especiales", "Se ven si deslizas a la izquierda.\nLas acciones especiales pueden ser eliminar o modificar el elemento, pero solo si tienes permisos ;)", null),
      PageViewModel("Permisos", "Para realizar acciones especiales debes tener permisos necesarios.\nSi estas en la zona de Kraalete, tendras permisos en todo.\nSi estas en Kraal, solo tendras permisos si eres el creador de la tarea, o "+
          "si yo, tu nuevo dios, te he marcado como administrador.", null),
      PageViewModel("Detalles", "Si pulsas sobre un elemento, veras los detalles.", null),
    ];


  static List<PageViewModel> tutorialTablon = [
      PageViewModel("Intro", "Aqui veras todas las tareas en las que te has metido.\n Como eres tan genial seguro que estara lleno.", null),
      PageViewModel("Prioridades", "Verde es que esta en proceso.\n Gris es que ha finalizado.", null),
      PageViewModel("Acciones principales", "Se ven si deslizas la tarea a la derecha.\nPuedes elegir salirte de la tarea como un cobarde, o puedes ser un heroe y marcarla como completada.\nPero completala antes porfi.", null),
      PageViewModel("Detalles", "Si pulsas sobre la tarea, veras los detalles.", null),
    ];

    static List<PageViewModel> tutorialListado = [
      PageViewModel("Intro", "Aqui podras ver todas las personas del grupo que esten en la aplicación", null),
      PageViewModel("Opciones", "Si pulsas sobre un elemento, o sobre la fecha, desplegaras el menu", null),
      PageViewModel("Llamada", "Si pulsas sobre el telefono, podras llamar a la persona.", null),
      PageViewModel("WhatsApp", "Si pulsas sobre el icono de WhatsApp, entraras en el chat de la persona.\nSiempre que tengas WhatsApp.", null),
    ];





    static void getTutorial(List<PageViewModel> lista, context){
    showDialog(
        context: context,
        child: IntroductionScreen(
          pages: lista,
          showSkipButton: true,
          skip: Text("Saltar"),
          done: Text("Listo"),
          next: Icon(Icons.arrow_forward),
          onDone: ()=>Navigator.of(context).pop(),
        )
    );
    }



}