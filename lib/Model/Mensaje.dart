import 'package:arkadia/Model/ObjectFather.dart';
import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Firebase.dart';
import 'package:arkadia/Utils/Operaciones.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:toast/toast.dart';
import 'package:uuid/uuid.dart';


/// Objeto que define un Mensaje
class Mensaje extends ObjectFather{
  @override
  String id;
  @override
  String nombre;
  @override
  Map<String,String> propietario;
  @override
  Cargo cargo;
  @override
  DateTime fechaCreacion;
  @override
  bool isKraal;

  /// Cuerpo del mensaje
  String _mensaje; 

  /// Constructor que permite crear un nuevo mensaje
  Mensaje(this.nombre, this._mensaje,  this.cargo, this.isKraal){
    this.id = Uuid().v4();
    this.propietario = LocalUser.localUser.getUser();
    this.fechaCreacion = DateTime.now();
  }

  /// Constructor que permite crear un mensaje a partir de un JSON
  Mensaje.fromJSON(Map json){
    this.id = json["id"];
    this.nombre = json["nombre"];
    this._mensaje = json["mensaje"];
    this.isKraal = json["isKraal"];
    this.propietario = ObjectFather.convertMap(json["propietario"]);
    this.cargo = Enums.getEnum(Cargo.values, json["cargo"]);
    this.fechaCreacion = json["fechaCreacion"];
  }

  String get mensaje => _mensaje;

  set mensaje(String value) {
    _mensaje = value;
  }

  @override
  toJson(){
    return{
      "id":this.id,
      "nombre":this.nombre,
      "propietario":this.propietario,
      "isKraal" : this.isKraal,
      "cargo": this.cargo.toString(),
      "fechaCreacion":this.fechaCreacion,
      "mensaje":_mensaje,
    };
  }

}

/// Widget que permite visualizar un mensaje
/// 
/// Crea un Widget de visualizacion a partir de [mensaje]
class MensajeCard extends StatefulWidget{
  Mensaje _mensaje;

  MensajeCard(this._mensaje);

  @override
  _MensajeCardState createState() => _MensajeCardState(_mensaje);
}

class _MensajeCardState extends State<MensajeCard> {

  Mensaje _mensaje;


  _MensajeCardState(this._mensaje);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      delegate: SlidableDrawerDelegate(),
      actionExtentRatio: 0.25,
      secondaryActions: Operaciones.getSubActions(_mensaje, context),
      child: GestureDetector(
        onTap: () =>
            showDialog(context: context,
                child: _createDialog()),
        child: Card(
          elevation: 20.0,
          color: Colors.blueAccent,
          margin: EdgeInsets.symmetric(vertical: 1),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Text(_mensaje.nombre,
                        style: TextStyle(fontSize: 30),
                        overflow: TextOverflow.ellipsis,),
                    ),
                    Text(_formateoFecha(_mensaje.fechaCreacion)),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(Enums.getCargo(_mensaje.cargo)),
                    Text(_mensaje.propietario.values.first),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Transforma a texto una fecha
  /// 
  /// Transforma un DateTime [fecha] a String
  String _formateoFecha(DateTime fecha) {
    if (fecha != null) {
      return formatDate(_mensaje.fechaCreacion, [dd, '-', mm, '-', yy]);
    } else {
      return "";
    }
  }

  /// Crea un Dialog con el detalle del mensaje
  /// 
  /// Devuelve un Widget con el detalle de un mensaje
  Widget _createDialog() {
    return Theme(
        data: ThemeData(
            dialogBackgroundColor: Colors.blueAccent),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            title: Text(_mensaje.nombre),
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Colors.white),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Creacion:"),
                        Text(_formateoFecha(_mensaje.fechaCreacion)),
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Propietario:"),
                        Text(_mensaje.propietario.values.first),
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Cargo:"),
                        Text(Enums.getCargo(_mensaje.cargo))
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      constraints: BoxConstraints(minWidth: double.infinity),
                      child: GestureDetector(
                        onLongPress: _showSnackBar,
                        child: Text(_mensaje.mensaje),
                      ),
                      decoration: BoxDecoration(
                          border: Border.all(
                            width: 5,
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        )
    );
  }

  void _showSnackBar() {
      Clipboard.setData(ClipboardData(text: _mensaje.mensaje));
      Toast.show("Copiado", context);
    }
}