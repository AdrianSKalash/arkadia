import 'package:arkadia/Model/ObjectFather.dart';
import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Enums.dart';
import 'package:arkadia/Utils/Operaciones.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:uuid/uuid.dart';
import 'package:date_format/date_format.dart';
import 'package:toast/toast.dart';

/// Objeto que define una tarea
class Tareas implements ObjectFather {
  @override
  String id;
  @override
  String nombre;
  @override
  Map<String, String> propietario;
  @override
  Cargo cargo;
  @override
  DateTime fechaCreacion;
  @override
  bool isKraal;

  /// Descripcion de la tarea
  String _descripcion;

  /// Contiene todos los responsables de la tarea
  Map<String, String> _responsables;

  /// Fecha en la que se ha completado la tarea
  DateTime _fechaFinalizacion;

  /// Fecha limite para realizar la tarea
  DateTime _fechaLimite;

  /// Prioridad de la tarea
  Prioridad _prioridad;

  /// Contructor para crear una nueva tarea
  Tareas(this.nombre, this._descripcion, this.cargo, this._prioridad,
      this._fechaLimite, this.isKraal) {
    this.id = Uuid().v4();
    this.propietario = LocalUser.localUser.getUser();
    this.fechaCreacion = DateTime.now();
    this._responsables = Map<String, String>();
  }

  /// Constructor para crear una nueva tarea a partir de un JSON
  Tareas.fromJSON(Map json) {
    this.id = json["id"];
    this.nombre = json["nombre"];
    this._descripcion = json["descripcion"];
    this.propietario = ObjectFather.convertMap(json["propietario"]);
    this.isKraal = json["isKraal"];
    this.cargo = Enums.getEnum(Cargo.values, json["cargo"]);
    this._responsables = ObjectFather.convertMap(json["responsables"]);
    this.fechaCreacion = json["fechaCreacion"];
    this._fechaFinalizacion = json["fechaFinalizacion"];
    this._fechaLimite = json["fechaLimite"];
    this._prioridad = Enums.getEnum(Prioridad.values, json["prioridad"]);
  }

  Prioridad get prioridad => _prioridad;

  DateTime get fechaFinalizacion => _fechaFinalizacion;

  DateTime get fechaLimite => _fechaLimite;

  Map<String, String> get responsables => _responsables;

  String get descripcion => _descripcion;

  set prioridad(Prioridad value) {
    _prioridad = value;
  }

  set fechaFinalizacion(DateTime value) {
    _fechaFinalizacion = value;
  }

  set fechaLimite(DateTime value) {
    _fechaLimite = value;
  }

  set descripcion(String value) {
    _descripcion = value;
  }

  @override
  toJson() {
    return {
      "id": this.id,
      "nombre": this.nombre,
      "propietario": this.propietario,
      "isKraal": this.isKraal,
      "cargo": this.cargo.toString(),
      "fechaCreacion": this.fechaCreacion,
      "fechaFinalizacion": _fechaFinalizacion,
      "fechaLimite": _fechaLimite,
      "responsables": _responsables,
      "descripcion": _descripcion,
      "prioridad": _prioridad.toString()
    };
  }
}

/// Widget que permite visualizar un mensaje
///
/// Crea un Widget de visualizacion a partir de [tarea].
/// Si [isGeneral] es [true], viene de un tablon general,
/// si es [false], viene del tablon personal.
class TareaCard extends StatefulWidget {
  Tareas _tarea;
  bool _isGeneral;

  TareaCard(this._tarea, this._isGeneral);

  @override
  _TareaCardState createState() => _TareaCardState(_tarea, _isGeneral);
}

class _TareaCardState extends State<TareaCard> {
  Tareas _tarea;
  bool _isGeneral;

  _TareaCardState(this._tarea, this._isGeneral);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      delegate: SlidableDrawerDelegate(),
      actionExtentRatio: 0.25,
      actions: Operaciones.getActions(_tarea, context, _isGeneral),
      secondaryActions:
          _isGeneral ? Operaciones.getSubActions(_tarea, context) : [],
      child: GestureDetector(
        onTap: () => showDialog(context: context, child: _createDialog()),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          elevation: 20.0,
          color: Enums.getPrioridad(_tarea.prioridad),
          margin: EdgeInsets.symmetric(vertical: 1),
          child: Padding(
            padding:
                const EdgeInsets.only(right: 15.0, bottom: 15.0, left: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Text(
                          _tarea.nombre,
                          style: TextStyle(fontSize: 30),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          _formateoFecha(_tarea.fechaCreacion),
                          style: TextStyle(fontSize: 11),
                        ),
                        _tarea.fechaLimite != null
                            ? Container(
                                padding: EdgeInsets.all(5.0),
                                margin: EdgeInsets.only(top: 5.0),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.black, width: 2.0),
                                    color: Colors.white),
                                child: Text(
                                  _formateoFecha(_tarea.fechaLimite),
                                  style: TextStyle(
                                      fontSize: 17,
                                      color: _comprobarFecha(_tarea.fechaLimite)
                                          ? Colors.red
                                          : Colors.black,
                                      fontWeight:
                                          _comprobarFecha(_tarea.fechaLimite)
                                              ? FontWeight.bold
                                              : FontWeight.normal),
                                ),
                              )
                            : Container()
                      ],
                    )
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(Enums.getCargo(_tarea.cargo)),
                    Text(_tarea.propietario.values.first),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Transforma a texto una fecha
  ///
  /// Transforma un DateTime [fecha] a String
  String _formateoFecha(DateTime fecha) {
    if (fecha != null) {
      return formatDate(fecha, [dd, '-', mm, '-', yy]);
    } else {
      return "";
    }
  }

  /// Crea un Dialog con el detalle del mensaje
  ///
  /// Devuelve un Widget con el detalle de un mensaje
  Widget _createDialog() {
    return Theme(
        data: ThemeData(
            dialogBackgroundColor: Enums.getPrioridad(_tarea.prioridad)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SimpleDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            title: Text(_tarea.nombre),
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Colors.white),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Inicio:"),
                        Text(_formateoFecha(_tarea.fechaCreacion)),
                        Divider(
                          height: 20,
                          color: Colors.black,
                        ),
                        Text("Fin:"),
                        Text(_formateoFecha(_tarea.fechaFinalizacion))
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Propietario:"),
                        Text(_tarea.propietario.values.first),
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Cargo:"),
                        Text(Enums.getCargo(_tarea.cargo))
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Fecha limite:"),
                        Text(_formateoFecha(_tarea.fechaLimite))
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Responsables:"),
                        Text(_getResponsables(_tarea.responsables))
                      ],
                    ),
                    Divider(
                      height: 20,
                      color: Colors.black,
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      constraints: BoxConstraints(minWidth: double.infinity),
                      child: GestureDetector(
                        onLongPress: _showSnackBar,
                        child: Text(_tarea.descripcion),
                      ),
                      decoration: BoxDecoration(
                          border: Border.all(
                        width: 5,
                      )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  void _showSnackBar() {
    if (_tarea.descripcion.isNotEmpty) {
      Clipboard.setData(ClipboardData(text: _tarea.descripcion));
      Toast.show("Copiado", context);
    }
  }

  /// Obtiene los responsables de la tarea
  ///
  /// Devuelve un [String] de los responsables obtienidos
  /// de [lista], separados por comas.
  String _getResponsables(Map<String, String> lista) {
    if (lista != null && lista.isNotEmpty) {
      List<String> names = List();
      for (var name in lista.values) {
        names.add(name);
      }
      return names.join(', ');
    } else {
      return "";
    }
  }

  bool _comprobarFecha(DateTime date) {
    return date.difference(DateTime.now()).inDays < 1;
  }
}
