import 'package:arkadia/Utils/Enums.dart';
import 'package:flutter/widgets.dart';
import 'package:uuid/uuid.dart';

/// Clase que contiene el usuario local
class LocalUser{
  /// Usuario local
  static User _localUser;

  static User get localUser {
    return _localUser;
  }

  static set localUser(User value) {
    _localUser = value;
  }



}

/// Clase que define un usuario
class User {
  /// Id del usuario
  String _idUser;
  /// Nombre del usuario
  String _nombre;
  /// Seccion a la que pertenece
  Seccion _seccion;
  /// Indica si tiene permisos de administrador
  bool _admin;

  String _foto;

  String _tlf;

  User.fromJSON(Map json){
    this._idUser = json["id"];
    this._nombre = json["nombre"];
    this._seccion = Enums.getEnum(Seccion.values, json["seccion"]);
    this._admin = json["admin"];
    this._tlf = json["tlf"];
  }

  ///Obtiene la identificacion de un Usuario
  ///
  ///Devuelve un [Map] con el 
  Map<String,String> getUser(){
    return {idUser : nombre};
  }

  bool get admin => _admin;

  Seccion get seccion => _seccion;

  String get nombre => _nombre;

  String get idUser => _idUser;

  String get foto => _foto!=null?_foto:"";

  String get tlf => _tlf;

  set foto(String value) {
    _foto = value;
  }

  toJson(){
    return{
      "id":_idUser,
      "admin":_admin,
      "nombre":_nombre,
      "seccion":_seccion.toString(),
      "tlf":_tlf,
    };
  }


}


