import 'package:arkadia/Model/User.dart';
import 'package:arkadia/Utils/Enums.dart';

/// Clase padre de los posibles objetos a crear
class ObjectFather {
  /// Id del objeto
  String _id;

  /// Nombre del objeto
  String _nombre;

  /// Cargo al que pertenece el objeto
  Cargo _cargo;

  /// Fecha de creacion del objeto
  DateTime _fechaCreacion;

  /// Persona propietaria del objeto
  ///
  /// Map creado a partir de id y nombre del usuario
  Map<String, String> _propietario;

  /// Si el objeto es de la parte de Kraal
  ///
  /// Devuelve [true] si es del Kraal y [false]
  /// si es del Kraalete
  bool _isKraal;

  String get id => _id;

  String get nombre => _nombre;

  DateTime get fechaCreacion => _fechaCreacion;

  Cargo get cargo => _cargo;

  set id(String value) {
    _id = value;
  }

  set cargo(Cargo value) {
    _cargo = value;
  }

  set nombre(String value) {
    _nombre = value;
  }

  Map<String, String> get propietario => _propietario;

  bool get isKraal => _isKraal;

  set isKraal(bool value) {
    _isKraal = value;
  }

  /// Permite transformar un objeto a JSON
  toJson() {}

  @override
  bool operator ==(o) {
    return o is ObjectFather && this.id == o.id;
  }

  /// Permite transformar un Map
  ///
  /// Transforma un [Map<dynamic,dynamic>] en un [Map<String,String>]
  static Map<String, String> convertMap(Map<dynamic, dynamic> old) {
    Map<String, String> map = new Map();
    if (old != null) {
      for (var key in old.keys) {
        map[key] = (old[key]);
      }
    }

    return map;
  }
}
